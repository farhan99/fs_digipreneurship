<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>

<style>
    #googleForm {
        text-align: center;
    }
</style>
<div class="main">
    <section class="home section" id="home">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="googleForm">
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdCksvQjb_PJyM_oBw_jJKeV8_dwBsABOTUAedIgo9_7_8ZGQ/viewform?embedded=true" width="720" height="1080" frameborder="0" marginheight="0" marginwidth="0">Memuat…</iframe>
                </div>
            </div>
        </div>
    </section>
</div>

<?= $this->endSection() ?>