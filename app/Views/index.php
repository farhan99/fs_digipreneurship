<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>

<div class="main">
    <section class="home section" id="home">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1>Rintis startupmu bersama mentor & praktisi digital</h1>
                </div>
                <div class="col-md-6">
                    <img src="<?= base_url('public/assets/svg/Scenes05.svg'); ?>" alt="" class="home-svg">
                </div>
            </div>
        </div>
    </section>

    <section class="text1 section" id="text1">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="https://lppm.unnes.ac.id/storage/2020/04/Metode-Penelitian-Lengkap.jpg" class="img-fluid" alt="...">
                </div>
                <div class="col-md-8">
                    <h3 class="h-text1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Exercitationem eum similique asperiores consequuntur veniam, doloribus obcaecati corrupti numquam maxime fugit?</h3>
                    <!-- <button type="button" class="btn btn-outline-success">Success</button> -->
                </div>
            </div>
        </div>
    </section>

    <section class="text2 section" id="text2">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur, magni.</h4>
                    <div class="card-group">
                        <div class="card">
                            <img src="..." class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="..." class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                        <div class="card">
                            <img src="..." class="card-img-top" alt="...">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?= $this->endSection() ?>